#!/bin/sh
pushd ..

# Build yasm

pushd yasm

if [ ! -d configure ]; then
  ./autogen.sh
fi

./configure

make
sudo make install

popd

popd
