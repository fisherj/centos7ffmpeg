#!/bin/sh
pushd ..

./yasm.sh
./sdl.sh
./ffmpeg.sh
./x264.sh
./x265.sh

popd

# Add the /usr/local directory to allow the .so files to be found.

sudo cp ffmpeg.conf /etc/ld.so.conf.d/ffmpeg.conf

sudo ldconfig
