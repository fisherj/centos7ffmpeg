#!/bin/sh
pushd ..

pushd SDL

hg checkout release-1.2.15

cp ../centos7ffmpeg/SDL_x11sym.h src/video/x11/.

if [ ! configure ]; then
  ./autogen.sh
fi

mkdir build

cd build

../configure

make
sudo make install

export USE_SDL=${PWD}/sdl-config

popd

popd
