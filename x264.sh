#!/bin/sh
pushd ..

# Build x264

pushd x264

./configure --enable-shared

make
sudo make install

popd

popd
