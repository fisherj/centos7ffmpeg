#!/bin/sh
pushd ..

# Build x265

pushd x265

cd build/linux

./make-Makefiles.bash

make
sudo make install

popd

popd
