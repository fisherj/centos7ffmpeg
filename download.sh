#!/bin/sh
pushd ..

# Install cmake as CentOS 7.0 does not appear to have it by default.

if sudo yum list installed cmake; then
  sudo yum -y install cmake
fi

# Install Mercurial as CentOS 7.0 does not appear to have it by default.

if sudo yum list installed hg; then
  sudo yum -y install hg
fi

# Install the x11 extras.

sudo yum install xorg-x11-*

# Grab Yasm

if [ ! -d yasm ]; then
  git clone git://github.com/yasm/yasm.git yasm
fi

# Grab the latest SDL source code.

if [ ! -d SDL ]; then
  hg clone http://hg.libsdl.org/SDL SDL
fi

# Grab the latest ffmpeg source code.

if [ ! -d ffmpeg ]; then
  git clone git://source.ffmpeg.org/ffmpeg.git ffmpeg
fi

# Grab the latest x264 source code.

if [ ! -d x264 ]; then
  git clone git://git.videolan.org/x264.git
fi

# Grab the latest x265 source code.

if [ ! -d x265 ]; then
  hg clone http://hg.videolan.org/x265
fi

popd
