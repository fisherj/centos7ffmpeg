#!/bin/sh
pushd ..

# Build ffmpeg.

pushd ffmpeg

# what about the following?

# --enable-fontconfig --enable-libfreetype
# --enable-libopenh264
# --enable-libx265

# The following configure is for a CentOS 7.0 build.

./configure --enable-gpl --enable-version3 --enable-shared --enable-libx264

make
sudo make install

popd

popd
